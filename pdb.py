# For saving, loading, importing & exporting databases

import os

class Database(object):

    def __init__(self, name, entries={}):
        """The class for all Peruse databases
        :name: name of the database
        :entries: the dictionary of entries
        """
        self.name = name
        self.entries = entries

def save_db(db, path="databases/"):
    """Saves a database to a given path.
    :db: the Python list containing all db entries
    :name: the name of the database
    :path: the path to the directory of all databases
    """
    if not (os.path.exists(path+db.name)):
        os.makedirs(path+db.name)
    for key in db.entries:
        with open("{}{}/{}.md".format(path,db.name,key), 
            "w+", encoding="utf-8") as f:
            f.write('\n'.join(db.entries[key]))
    
def load_db(path):
    """Loads a database from a given path.
    :path: the path to the directory of the database
    """
    if not (os.path.exists(path)):
        raise Exception("Path \"{}\" does not exist".format(path))

    name = path.split("/")[-1]
    db = Database(name)

    for file in os.listdir(path):
        with open(path+"/"+file, "r", encoding="utf8") as f:
            data = f.readlines()
            data = [x.replace('\n','') for x in data]
            db.entries[file[:-3]] = data
    
    return db



